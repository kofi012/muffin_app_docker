# Muffin Docker Compose APP

This will be an app to showcase docker compose. We'll build it with a php html front page and another container with a python API simulating a DB.

We'll use this class to cover:

- Dev Environments
- Testing & CD environments
- Git and github branching
- Git branching practices and git etiquette
- Docker and docker compose

Will also touch on:

- Python
- API
- Webapps
- Separation of concerns

Extra objectives:

- Extra objectives:

### Plan to complete

1. Create a simple API with python. Put it in a conatiner.
2. Create a simple PHP app to parse/consume JSON sent from api.
3. Build our docker compose